# Translation of tabswitcherplugin to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-19 01:01+0000\n"
"PO-Revision-Date: 2014-09-14 22:04+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: tabswitcher.cpp:49
#, kde-format
msgid "Document Switcher"
msgstr "Dokumentbytter"

#: tabswitcher.cpp:94
#, kde-format
msgid "Last Used Views"
msgstr "Sist brukte visninger"

#: tabswitcher.cpp:97
#, kde-format
msgid "Opens a list to walk through the list of last used views."
msgstr "Åpner en liste for å bla gjennom lista over sist brukte visninger."

#: tabswitcher.cpp:98 tabswitcher.cpp:106
#, kde-format
msgid "Walk through the list of last used views"
msgstr "Bla gjennom lista over sist brukte visninger"

#: tabswitcher.cpp:102
#, kde-format
msgid "Last Used Views (Reverse)"
msgstr "Sist brukte visninger (baklengs)"

#: tabswitcher.cpp:105
#, kde-format
msgid "Opens a list to walk through the list of last used views in reverse."
msgstr ""
"Åpner en liste for å bla baklengs gjennom lista over sist brukte visninger."

#: tabswitcher.cpp:110
#, kde-format
msgid "Close View"
msgstr ""

#: tabswitcher.cpp:113 tabswitcher.cpp:114
#, kde-format
msgid "Closes the selected view in the list of last used views."
msgstr ""

#. i18n: ectx: Menu (go)
#: ui.rc:6
#, kde-format
msgid "&Go"
msgstr ""
